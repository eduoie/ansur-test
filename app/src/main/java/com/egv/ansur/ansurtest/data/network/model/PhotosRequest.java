package com.egv.ansur.ansurtest.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotosRequest {
    @SerializedName("data")
    @Expose
    public Data data;
}
