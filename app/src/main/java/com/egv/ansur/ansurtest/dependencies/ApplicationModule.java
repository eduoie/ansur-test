package com.egv.ansur.ansurtest.dependencies;


import android.app.Application;

import com.egv.ansur.ansurtest.BuildConfig;
import com.egv.ansur.ansurtest.data.network.ApiService;
import com.egv.ansur.ansurtest.data.network.AuthInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class provides dependencies used by the app
 */
@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(String baseUrl) {
        OkHttpClient client;
        client = new OkHttpClient.Builder()
                .addInterceptor(new AuthInterceptor("testcandidate", "bygcd33"))
                .addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    ApiService provideApiCalls() {
        return provideRetrofit(BuildConfig.SERVICE_ENDPOINT).create(ApiService.class);
    }

}
