package com.egv.ansur.ansurtest.data;


import com.egv.ansur.ansurtest.data.model.Photo;
import com.egv.ansur.ansurtest.data.network.ApiService;
import com.egv.ansur.ansurtest.data.network.model.PhotosRequest;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This class acts as the single Repository for the app to retrieve data.
 */
@Singleton
public class Repository {

    public static final int QUERY_ITEMS_LIMIT = 25;
    public static final String QUERY_FIELDS = "location_latitude,location_longitude,thumbnail_url";

    ApiService apiService;

    @Inject
    public Repository(ApiService apiService) {
        this.apiService = apiService;
    }

    /**
     * Async call to retrieve the photos data (location, url, etc), but doesn't retrieve the actual photo
     * @param callback
     */
    public void getPhotos(final ImagesDataLoadedCallback callback) {
        // TODO: not working properly when limiting the fields on the query. Thumbnails are not found.
        //        apiService.getListPhotos(QUERY_ITEMS_LIMIT, QUERY_FIELDS)
        apiService.getFullListPhotos()
                .enqueue(new Callback<PhotosRequest>() {
                    @Override
                    public void onResponse(Call<PhotosRequest> call, Response<PhotosRequest> response) {
                        callback.onImagesDataLoaded(response.body().data.photos);
                    }

                    @Override
                    public void onFailure(Call<PhotosRequest> call, Throwable t) {
                        callback.onImagesDataLoadedError(t);
                    }
                });
    }

    public interface ImagesDataLoadedCallback {
        void onImagesDataLoaded(List<Photo> photoList);

        void onImagesDataLoadedError(Throwable throwable);
    }

}
