package com.egv.ansur.ansurtest.data.network.model;

import com.egv.ansur.ansurtest.data.model.Photo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("photos")
    @Expose
    public List<Photo> photos = null;
}
