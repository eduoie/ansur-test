package com.egv.ansur.ansurtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.egv.ansur.ansurtest.data.Repository;
import com.egv.ansur.ansurtest.data.model.Photo;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements GridImagesAdapter.ItemClickListener, Repository.ImagesDataLoadedCallback {

    public static final int SPAN_COUNT = 3;

    @BindView(R.id.imagesRecyclerView)
    RecyclerView imagesRecyclerView;

    @Inject
    Repository repository;
    private GridImagesAdapter gridImagesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((BaseApp) getApplication()).getComponent().inject(this);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        imagesRecyclerView.setLayoutManager(new GridLayoutManager(this, SPAN_COUNT));
        gridImagesAdapter = new GridImagesAdapter();
        gridImagesAdapter.setItemClickListener(this);
        imagesRecyclerView.setAdapter(gridImagesAdapter);

        repository.getPhotos(this);
    }

    @Override
    public void onItemClick(Photo photo) {
        Intent intent = new Intent(this, MapsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", photo.location_latitude);
        bundle.putDouble("lng", photo.location_longitude);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onImagesDataLoaded(List<Photo> photoList) {
        gridImagesAdapter.setPhotoList(photoList);
    }

    @Override
    public void onImagesDataLoadedError(Throwable throwable) {
        Toast.makeText(this, "Error loading images. Sorry :(", Toast.LENGTH_SHORT).show();
    }
}
