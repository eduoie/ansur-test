package com.egv.ansur.ansurtest;


import com.egv.ansur.ansurtest.dependencies.ApplicationComponent;
import com.egv.ansur.ansurtest.dependencies.ApplicationModule;
import com.egv.ansur.ansurtest.dependencies.DaggerApplicationComponent;

public class BaseApp extends android.app.Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    /**
     * Used by those classes that cannot inject dependencies through constructors, such as activities
     */
    public ApplicationComponent getComponent() {
        return component;
    }

}
