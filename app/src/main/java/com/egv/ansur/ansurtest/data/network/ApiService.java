package com.egv.ansur.ansurtest.data.network;

import com.egv.ansur.ansurtest.data.network.model.PhotosRequest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * This interface be passed to Retrofit to generate the network requests that will be handled by the repository.
 */
public interface ApiService {

    /**
     * TODO: There seems to be a problem with thumbnail retrieval when using the fields parameter. Thumbnail is not found
     * @param limit
     * @param fields
     * @return
     */
    @GET("api/photos.json")
    Call<PhotosRequest> getListPhotos(@Query("limit") int limit, @Query("fields") String fields);

    /**
     * Using this method instead to retrieve the list of photos
     */
    @GET("api/photos.json")
    Call<PhotosRequest> getFullListPhotos();

}
