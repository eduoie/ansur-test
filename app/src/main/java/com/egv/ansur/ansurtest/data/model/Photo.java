package com.egv.ansur.ansurtest.data.model;

public class Photo {

    public String thumbnail_url;
    public double location_latitude;
    public double location_longitude;

    public Photo(String url, double lat, double lng) {
        this.thumbnail_url = url;
        this.location_latitude = lat;
        this.location_longitude = lng;
    }
}
