package com.egv.ansur.ansurtest.dependencies;

import com.egv.ansur.ansurtest.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(MainActivity target);

}
